const axios = require('axios');
const { parseIdFromUrl } = require('../utils');
const redis = require('./redis');

/** This is a super awesomely named function */
function formatResponse(obj) {
  return { ...obj, id: parseIdFromUrl(obj.url) };
}

function people() {
  return get('people/')
    .then(({ results: people }) => people.map(formatResponse));
}

function film(id) {
  return get(`films/${id}`)
    .then(formatResponse);
}

function starship(id) {
  return get(`starships/${id}`)
    .then(formatResponse);
}

function person(id) {
  return get(`people/${id}`)
    .then(formatResponse);
}

async function get(url) {
  const cache = await redis.getAsync(url);
  if (cache) { return JSON.parse(cache); };

  return axios({ url, baseURL: process.env.SWAPI_URL })
    .then(({ data }) => {
      redis.setAsync(url, JSON.stringify(data));
      return data;
    })
    .catch((e) => { 
      console.log(e);
      throw new Error(`Failed GET ${url}`) 
    });
}

module.exports = {
  film,
  people,
  person,
  starship,
}