const DataLoader = require('dataloader');
const swapi = require('../services/swapi');
const { parseIdFromUrl, mapIdsToFn } = require('../utils');

const filmsLoader = new DataLoader((ids) => mapIdsToFn(ids, swapi.film));
const starshipLoader = new DataLoader((ids) => mapIdsToFn(ids, swapi.starship));

module.exports = {
  films: async ({ films }) => films.map(
    (filmUrl) => filmsLoader.load(parseIdFromUrl(filmUrl))
  ),
  starships: async ({ starships }) => starships.map(
    (starshipUrl) => starshipLoader.load(parseIdFromUrl(starshipUrl))
  ),
}