const { 
  film,
  people,
  person,
  starship,
} = require('../services/swapi');

module.exports = {
  film: (parent, { id }) => film(id),
  people,
  person: (parent, { id }) => person(id),
  starship: (parent, { id }) => starship(id),
};
