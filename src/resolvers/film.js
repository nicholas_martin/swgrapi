const DataLoader = require('dataloader');
const swapi = require('../services/swapi');
const { parseIdFromUrl, mapIdsToFn } = require('../utils');

const peopleLoader = new DataLoader((ids) => mapIdsToFn(ids, swapi.person));

module.exports = {
  characters: async ({ characters }) => characters.map(
    (personUrl) => peopleLoader.load(parseIdFromUrl(personUrl))
  ),
};