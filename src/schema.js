const { gql } = require('apollo-server');

const baseType = `
    id: ID!
`;

const types = `
  type Person {
    ${baseType}
    name: String!
    films: [Film]!
    starships: [Starship!]
  }

  type Film {
    ${baseType}
    title: String!
    starships: [Starship!]
    characters: [Person!]
  }

  type Starship {
    ${baseType}
    model: String!
    name: String!
  }
`;

const queries = `
  type Query {
    people: [Person]!
    person(id: ID!): Person
    film(id: ID!): Film
    starship(id: ID!): Starship
  }
`;

const typeDefs = gql`
  ${types}
  ${queries}
`;

module.exports = {
  typeDefs,
};
