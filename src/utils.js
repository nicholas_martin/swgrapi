function parseIdFromUrl(url) {
  return url.slice(0, -1).split('/').pop();
}

function mapIdsToFn(ids, fn) {
  return Promise.all(ids.map(id => fn(id)))
}

module.exports = {
  mapIdsToFn,
  parseIdFromUrl,
}