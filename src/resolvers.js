const Query = require('./resolvers/query');

const Film = require('./resolvers/film');
const Person = require('./resolvers/person');

module.exports = {
  Query,
  Film,
  Person,
};
