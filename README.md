# GraphQL SWAPI

* In the swgrapi/src directory, run `npm install`
* Back in the swgrapi directory, run `docker-compose up`
* Go to `http://localhost:4000/` to use playground

## Next steps
* Pagination: Right now, the api only consumes the first page of each SWAPI request
* Finish schema

## Example request:
```
query {
  people {
    id
    name
    films {
      id
      title
      characters {
        name
      }
    }
    starships {
      id
      name
      model
    }
  }
}
```